<?php
require_once('poruke.php');
require_once('filter.php');
require_once('biznis.php');

//spremanje korisnika
function f_spremi_korisnika($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;

    echo "Tu sam!";
    if(f_check_korisnici($in_obj)){
        echo "Tu sam 1!";
        return;
    }
    echo "Tu sam 2!";
    if(b_check_korisnici($db,$in_obj)){
        echo "Tu sam 3!";
        return;
    }


        if (isset($in_obj->ID)){
        $sql = "UPDATE korisnici SET "
          . "ime = '" . $in_obj->IME ."', "
          . "prezime = '" . $in_obj->PREZIME ."', "
          . "email = '" . $in_obj->EMAIL."', "
          . "password = " . $in_obj->PASSWORD .", "
          . "ulica = " . $in_obj->ULICA .", "
          . "kucbr = " . $in_obj->KUCBR .", "
          . "dodkucbr = " . $in_obj->DODKUCBR .", "
          . "grad = " . $in_obj->GRAD .", "
          . "pozmob = " . $in_obj->POZMOB .", "
	      . "updby = NOW()"  
          . " WHERE ID = "  . $in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO korisnici (ime, prezime, email, password, ulica, kucbr, dodkucbr, grad, pozmob, brmob,lastupd) VALUES "
	      . "( " 
	      . "'". $in_obj->IME . "'" . "," 
	      . "'". $in_obj->PREZIME . "'" . "," 
	      . "'". $in_obj->EMAIL . "'" . "," 
          . "'". $in_obj->PASSWORD . "'" . "," 
	      . "'". $in_obj->ULICA . "'" . "," 
	      . "'". $in_obj->KUCBR . "'" . "," 
          . "'". $in_obj->DODKUCBR . "'" . "," 
	      . "'". $in_obj->GRAD . "'" . "," 
	      . "'". $in_obj->POZMOB . "'" . ","
          . "'". $in_obj->BRMOB. "'" . ","
          . "NOW()"
          .")"; 
           //echo $sql;

          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          } 
    }
}

//brisanje korisnika
function f_spremi_korisnika_delete($db, $in_obj){
    global $delete_error;
    global $delete_pass;
 
    $sql = "DELETE from korisnici"  
           . " WHERE ID = "  . $in_obj->ID;
 
    if ($db->query($sql) === TRUE) {
        echo $delete_pass;
    }else{
        echo $delete_error;   
    } 
 
 }

//spremanje kategorije


function f_spremi_kategorije($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;

   
    if(f_check_kategorije($in_obj)){
        return;
    }
  

    if(b_check_kategorije($db,$in_obj)){
        return;
    }


        if (isset($in_obj->ID)){
        $sql = "UPDATE kategorije SET "
          . "naziv = '" . $in_obj->NAZIV ."', "
	      . "lastupd = NOW()"  
          . " WHERE ID = "  . $in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO kategorije (naziv,lastupd) VALUES "
	      . "( " 
	      . "'". $in_obj->NAZIV . "'" . "," 
          . "NOW()"
          .")"; 
           //echo $sql;

          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          } 
    }
}

//brisanje kategorije
function f_spremi_kategorije_delete($db, $in_obj){
    global $delete_error;
    global $delete_pass;
 
    $sql = "DELETE from kategorije" 
           . " WHERE ID = "  . $in_obj->ID;
 
    if ($db->query($sql) === TRUE) {
        echo $delete_pass;
    }else{
        echo $delete_error;   
    } 
 
 }

 
//spremanje zaposlenika
 function f_spremi_zaposlenika($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;

    if(f_check_zaposlenici($in_obj)){
        return;
    }
    if(b_check_zaposlenici($db,$in_obj)){
        return;
    }

        if (isset($in_obj->ID)){
        $sql = "UPDATE zaposlenici SET "
          . "ime = '" . $in_obj->IME ."', "
          . "prezime = '" . $in_obj->PREZIME ."', "
          . "OIB = '" . $in_obj->OIB ."', "
          . "pozMob = " . $in_obj->POZMOB .", "
          . "brMob = " . $in_obj->BRMOB .", "
          . "radMj = " . $in_obj->RADMJ .", "
          . "email = " . $in_obj->EMAIL .", "
          . "datRod = " . $in_obj->DATROD .", "
          . "datZap = " . $in_obj->DATZAP .", "
          . "IDposlovnice = " . $in_obj->IDPOSLOVNICE .", "
	      . "LastUpd = NOW()" 
          . " WHERE ID = "  . $in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO zaposlenici (ime, prezime, OIB, pozMob, brMob, radMj, email, datRod, datZap, IDposlovnice, lastupd) VALUES "
	      . "( " 
	      . "'". $in_obj->IME . "'" . "," 
	      . "'". $in_obj->PREZIME . "'" . "," 
	      . "'". $in_obj->OIB . "'" . "," 
          . "'". $in_obj->POZMOB . "'" . "," 
	      . "'". $in_obj->BRMOB . "'" . "," 
	      . "'". $in_obj->RADMJ . "'" . "," 
          . "'". $in_obj->EMAIL . "'" . "," 
	      . "'". $in_obj->DATROD . "'" . "," 
	      . "'". $in_obj->DATZAP . "'" . ","
          . "'". $in_obj->IDPOSLOVNICE . "'" . ","
          . "NOW()"
          .")"; 
           //echo $sql;

          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          } 
    }
}

// brisanje zaposlenika

function f_spremi_zaposlenika_delete($db, $in_obj){
    global $delete_error;
    global $delete_pass;
 
    $sql = "DELETE from zaposlenici"
        . " WHERE OIB = "  . $in_obj->OIB;
 
    if ($db->query($sql) === TRUE) {
        echo $delete_pass;
    }else{
        echo $delete_error;   
    } 
 
 }

 //spremi proizvod

function f_spremi_proizvod($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;

    if(f_check_proizvod($in_obj)){
        return;
    }
    if(b_check_proizvod($db,$in_obj)){
        return;
    }

        if (isset($in_obj->ID)){
        $sql = "UPDATE proizvod SET "
          . "idvrproiz = '" . $in_obj->IDVRPROIZ ."', "
          . "brand = '" . $in_obj->BRAND ."', "
          . "opis = '" . $in_obj->OPIS ."', "
          . "naziv = '" . $in_obj->NAZIV ."', "
          . "cijena = " . $in_obj->CIJENA .", "
          . "CreDate = NOW()" .","
          . "updby = '". $_SESSION['EMAIL']."'";
          
        
        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO proizvod (idvrproiz,brand,opis,naziv,cijena,CreDate,creby) VALUES "
	      . "( " 
	      . "'". $in_obj->IDVRPROIZ . "'" . "," 
	      . "'". $in_obj->BRAND . "'" . "," 
	      . "'". $in_obj->OPIS . "'" . "," 
          . "'". $in_obj->NAZIV . "'" . "," 
	      . "'". $in_obj->CIJENA . "'" . ","
          . "NOW()". ","
          . "'".$_SESSION['EMAIL']. "'"
          .")" ; 
          // echo $sql;

          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          } 
    }
}

//obrisi proizvod
function f_spremi_proizvod_delete($db, $in_obj){
    global $delete_error;
    global $delete_pass;
 
    $sql = "DELETE from proizvod"
        . " WHERE ID = "  . $in_obj->ID;
 
    if ($db->query($sql) === TRUE) {
        echo $delete_pass;
    }else{
        echo $delete_error;   
    } 
 
 }


 
?>