<?php
function f_get_database(){
    $db = new mysqli(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
    if($db->connect_errno){
        throw new Exception("Neuspješna konekcija na bazu");
    }
    $db->set_charset("utf8");
    return $db;
}

//funkcija za provjeru logina
function f_login($db, $in_obj){
    $sql = 'SELECT * FROM korisnici WHERE EMAIL =\'' . $in_obj->username . "' AND PASSWORD='" . $in_obj->password ."'";
    $rows=[];
    $result = $db->query($sql);
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }

    if (!empty($rows)){
        $_SESSION = $rows[0];
        echo json_encode($rows);
    }else{
        global $wrong_login;
        echo $wrong_login;
    }
}

function f_get_count($db, $sql){
    $result = $db->query($sql);
    $row = mysqli_fetch_assoc($result);
    return $row['count(1)'];
}

function f_get_rows($db, $sql){
    $db->set_charset("utf8");    
    $result = $db->query($sql);
    $rows=[];
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }    
    return $rows; 
}

//provjera datuma
function isValid($date, $format = 'Y-m-d'){
    $dt = DateTime::createFromFormat($format, $date);
    return $dt && $dt->format($format) === $date;
  }

?>