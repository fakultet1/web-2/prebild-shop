<?php
require_once('poruke.php');
require_once('database.php');


//provjera zaposlenika
function f_check_zaposlenici($in_obj){
    global $error_101;
    global $error_102;
    global $error_103;
    global $error_104;
    global $error_105;
    global $error_106;
    global $error_107;
    global $error_108;
    global $error_109;
    global $error_110;
    global $error_111;
    global $error_112;
    global $error_133;
    $pozmob_array=array("99", "98", "97", "95", "92", "91");
    $radnomj_array=array("PRODAVAC", "DIREKTOR", "VODITELJ_POSLOVNICE");

    global $has_error;
    if($in_obj->IME ==' ' || !isset($in_obj->IME)){
        $has_error=true;
        echo $error_101;
    }
    if($in_obj->PREZIME ==' ' || !isset($in_obj->PREZIME)){
        $has_error=true;
        echo $error_102;
    }
    if($in_obj->OIB ==0 || !isset($in_obj->OIB)){
        $has_error=true;
        echo $error_103;
    }
    if(strlen($in_obj->OIB) !=11 || !isset($in_obj->OIB)){
        $has_error=true;
        echo $error_104;
    }
    if($in_obj->POZMOB ==0 || !isset($in_obj->POZMOB)){
        $has_error=true;
        echo $error_105;
    }if(!in_array($in_obj->POZMOB, $pozmob_array)){
        $has_error=true;
        echo $error_106;
    }
    if($in_obj->BRMOB ==0 || !isset($in_obj->BRMOB)){
        $has_error=true;
        echo $error_107;
    }
    if(strlen($in_obj->BRMOB) > 9 || !isset($in_obj->BRMOB)){
        $has_error=true;
        echo $error_133;
    }
    if(!in_array($in_obj->RADMJ, $radnomj_array)){
        $has_error=true;
        echo $error_108;
    }
    if($in_obj->EMAIL ==' ' || !isset($in_obj->EMAIL)){
        $has_error=true;
        echo $error_109;
    }
    if($in_obj->IDPOSLOVNICE ==0 || !isset($in_obj->IDPOSLOVNICE)){
        $has_error=true;
        echo $error_110;
    }
    if($in_obj->DATROD ==' ' || !isset($in_obj->DATROD)){
        $has_error=true;
        echo $error_111;
    }
    if((!isValid($in_obj->DATROD))){
        $has_error=true;
        echo $error_112;
    }

    return $has_error;
}


//provjera proizvoda
function f_check_proizvod($in_obj){
    global $error_113;
    global $error_114;
    global $error_115;
    global $error_116;
    global $error_131;

    global $has_error;
    if($in_obj->BRAND ==' ' || !isset($in_obj->BRAND)){
        $has_error=true;
        echo $error_113;
    }
    if($in_obj->OPIS ==' ' || !isset($in_obj->OPIS)){
        $has_error=true;
        echo $error_114;
    }
    if($in_obj->NAZIV ==' ' || !isset($in_obj->NAZIV)){
        $has_error=true;
        echo $error_115;
    }
    if($in_obj->CIJENA ==0 || !isset($in_obj->CIJENA)){
        $has_error=true;
        echo $error_116;
    }if(!is_numeric($in_obj->CIJENA)){
        echo $error_131;
    }
    

    return $has_error;
}


//provjera kategorije
function f_check_kategorije($in_obj){
    global $error_117;
    
    global $has_error;
    if($in_obj->NAZIV ==' ' || !isset($in_obj->NAZIV)){
        $has_error=true;
        echo $error_117;
    }

    return $has_error;

}


//provejra korisnika

function f_check_korisnici($in_obj){
    global $error_118;
    global $error_119;
    global $error_120;
    global $error_121;
    global $error_122;
    global $error_123;
    global $error_124;
    global $error_125;
    global $error_126;
    global $error_127;
    global $error_140;
    
    $pozmob_array=array("99", "98", "97", "95", "92", "91");

    global $has_error;
    if($in_obj->IME ==' ' || !isset($in_obj->IME)){
        $has_error=true;
        echo $error_118;
    }
    if($in_obj->PREZIME ==' ' || !isset($in_obj->PREZIME)){
        $has_error=true;
        echo $error_119;
    }
    if($in_obj->EMAIL ==' ' || !isset($in_obj->EMAIL)){
        $has_error=true;
        echo $error_120;
    }
    if($in_obj->PASSWORD ==' ' || !isset($in_obj->PASSWORD)){
        $has_error=true;
        echo $error_121;
    }
    if($in_obj->ULICA ==' ' || !isset($in_obj->ULICA)){
        $has_error=true;
        echo $error_122;
    }
    if(is_numeric($in_obj->KUCBR)){
        $has_error=true;
        echo $error_140;
    }
    if($in_obj->KUCBR ==0 || !isset($in_obj->KUCBR)){
        $has_error=true;
        echo $error_123;
    }
    if($in_obj->GRAD ==' ' || !isset($in_obj->GRAD)){
        $has_error=true;
        echo $error_124;
    }
    echo 'evo';
    if(!in_array($in_obj->POZMOB, $pozmob_array)){
        echo 'test';
        $has_error=true;
        echo $error_125;
        return true;
    }
    if($in_obj->BRMOB ==0 || !isset($in_obj->BRMOB)){
        $has_error=true;
        echo $error_126;
    }
    if(strlen($in_obj->BRMOB) > 9 || !isset($in_obj->BRMOB)){
        $has_error=true;
        echo $error_127;
    }
}

?>